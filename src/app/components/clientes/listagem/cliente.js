import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions';

class Cliente extends React.Component{
    state = {
        iniciouExclusao: false
    }

    alterarCliente = () =>{
        this.props.setClienteParaAlterar(this.props.cliente);
    }

    excluirCliente = () =>{
        const { iniciouExclusao } = this.state;
        if(!iniciouExclusao) return this.setState({ iniciouExclusao: true })
        this.props.removeCliente(this.props.cliente.id);
    }

    render(){
        const { cliente } = this.props;
        const { iniciouExclusao } = this.state;
        return(
            <tr>
                <td>{cliente.nome}</td>
                <td>{cliente.telefone}</td>
                <td>{cliente.email}</td>
                <td>{cliente.cpf}</td>
                <td onClick={this.alterarCliente}>{"Alterar"}</td>
                <td onClick={this.excluirCliente}>{ iniciouExclusao ? "Certeza?" : "Excluir"}</td>
            </tr>
        )
    }
}

export default connect(null, actions)(Cliente)