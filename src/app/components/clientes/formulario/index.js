import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../actions';

const Input  = (props) => {
    return (
        <div>
            {props.label && <label>{props.label}</label>}
            <input value={props.value} onChange={props.onChange} placeholder={props.placeholder} type={props.type || "text"} />
            { props.erro && <small>{props.erro}</small>} 
        </div>
    )
}

const DEFAULT_STATE = {
    form: {
        nome: "",
        email: "",
        telefone: "",
        cpf: ""
    },
    erros: {},
    mostrarForm: false
}

class Formulario extends React.Component{

    state = DEFAULT_STATE;

    componentWillUpdate(nextProps){
        if(!this.props.cliente && nextProps.cliente){
            this.setState({
                form : nextProps.cliente,
                erros: {},
                mostrarForm: true
            })
        }

        if(this.props.cliente && !nextProps.cliente){
            this.setState(DEFAULT_STATE);
        }
    }

    validar = () =>{
        const { form } = this.state;
        const erros = {};
        ["nome","telefone","email","cpf"].forEach((item) => {
            if(!form[item]) erros[item] = "Digite o " + item;
        });
        this.setState({erros});
        return Object.keys(erros).length === 0;
    }

    mostrarForm = () => {
        this.setState({ mostrarForm: !this.state.mostrarForm })
        console.log(this.state.mostrarForm)
    }

    onChange = (field, ev) => {
        const { form } = this.state;
        form[field] = ev.target.value;
        this.setState({ form }, () =>{
            this.validar()
        });
    }

    handleSubmit = () =>{
        if(!this.validar()) return null;
        const { form } = this.state;
        const { cliente } = this.props;
        if(cliente){
            this.props.updateCliente(cliente.id, form);
        }else{
            this.props.addCliente(form);
        }
               
        this.setState(DEFAULT_STATE);
    }

    renderFormulario(){
        const { form, erros } = this.state;
        return (
            <div className="Formulario">
                {console.log(this.state.mostrarForm)}
                <div>
                    <Input value={form.nome} label={"Nome"} onChange={(ev) => this.onChange('nome', ev)} erro={erros.nome} />
                    <Input value={form.telefone} label={"Telefone"} onChange={(ev) => this.onChange('telefone', ev)} erro={erros.telefone} />
                    <Input value={form.cpf} label={"CPF"} onChange={(ev) => this.onChange('cpf', ev)} erro={erros.cpf} />
                    <Input value={form.email} label={"Email"} onChange={(ev) => this.onChange('email', ev)} type={"email"} erro={erros.email} />
                    <div>
                        <button onClick={this.handleSubmit}>
                            Salvar
                        </button>
                    </div>
                </div>
            </div>
        )
    }

    renderBotao(){
        return(
            <div>
                {console.log(this.state.mostrarForm)}
                <button onClick={this.mostrarForm}>
                    Adicionar Cliente
                </button>
            </div>
        )
    }

    render(){
        return (
            this.state.mostrarForm ? this.renderFormulario() : this.renderBotao()
        )
    }
}

const mapStateToProps = state => ({
    cliente: state.clientes.cliente
})

export default connect(mapStateToProps, actions)(Formulario)